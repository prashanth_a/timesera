<!DOCTYPE html>

<html lang="en">
    <?php include_once('includes/header.php');?>

    <style type="text/css">
        .product_bg {
            box-shadow: 1px 1px 17px 1px #dedede;
            border-radius: 5px;
            background-color: #fff;
            padding: 8px;
        }
        i.fas.fa-star,
        i.fas.fa-star-half-alt {
            color: #ff5200;
        }
        span s {
            font-size: 12px;
            margin-left: 10px;
            color: #757575;
        }
        .pdtype {
            font-size: 14px;
            line-height: 10px;
        }
        .prize {
            font-weight: bold;
            text-align: right;
        }
        .count {
            background: #e6e6e6;
            padding: 3px 7px;
            border-radius: 3px;
        }
        .add,
        .sub {
            font-size: 12px;
            color: #0b0157;
        }
        .pos-rel {
            position: relative;
        }
        .coupon-code {
            height: 37px;
            border-radius: 5px;
            border: 1px solid #0b0157;
        }
        .apply-code {
            background-color: #0b0157;
            color: #fff;
            position: absolute;
            /*right: 75px;*/
            font-size: 13px;
            margin-left: -35px;
        }
        .apply-code:hover,
        .apply-code:focus {
            color: #fff;
        }
        .delete-item {
            background-color: red;
            color: #ffffff;
            position: absolute;
            font-size: 13px;
            left: 227px;
        }
        .cart-bg {
            box-shadow: 1px 1px 17px 1px #dedede;
            padding: 0px 10px;
            background: #fff;
            border-radius: 5px;
        }
        .total {
            padding: 8px 10px;
            border-bottom: 1px solid;
        }
        .g-total {
            padding: 8px 10px;
            border: none;
        }
        i.far.fa-circle,
        i.fas.fa-circle {
            font-size: 10px;
            padding-right: 5px;
        }
        .check-btn {
            width: 100%;
            background-color: #0b0157;
            color: #fff;
            text-align: center;
            border-radius: 5px;
            margin-top: 20px;
            padding: 10px;
        }

        
    </style>

    <body>
        <!-- Main Wrapper -->

        <div class="main-wrapper">
            <!-- Header -->

            <?php include_once('includes/topbar.php');?>

            <!-- /Header -->

            <!-- Sidebar -->

            <?php include_once('includes/sidebar.php');?>

            <!-- /Sidebar -->

            <!-- Page Wrapper -->

            <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="my-2"><i class="fas fa-chevron-left"></i></div>
                            <div class="my-2">checkout</div>
                        </div>
                        <div class="col-12">
                            <div class="product_bg">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mx-auto d-block text-center bg-light">
                                            <img src="images/men.png" width="70px" class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-4 px-0 mt-2">
                                        <div class="pdtype">T-Shirt</div>
                                        <div>Men's Style</div>
                                        <div>Size - L</div>
                                        <div class="pdtype">Color - White</div>
                                    </div>
                                    <div class="col-4">
                                        <h5 class="prize">$80.00</h5>
                                        <div class="text-right">
                                            <a href="#"><i class="fas fa-minus sub pr5"></i></a>
                                            <span class="count">1</span>
                                            <a href="#"><i class="fas fa-plus add pl5"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt15">
                            <div class="pos-rel">
                                <input type="text" name="" placeholder="Coupon Code" class="coupon-code" />

                                <div class="btn btn-lg apply-code">Apply</div>

                                <div class="btn btn-lg delete-item">Delete item</div>
                            </div>
                        </div>
                        <div class="col-12 my-3">
                            <div class="product_bg">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mx-auto d-block text-center bg-light">
                                            <img src="images/shoes/shoe.png" width="70px" class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-4 px-0 mt-2">
                                        <div class="pdtype">T-Shirt</div>
                                        <div>Men's Style</div>
                                        <div>Size - L</div>
                                        <div class="pdtype">Color - White</div>
                                    </div>
                                    <div class="col-4">
                                        <h5 class="prize">$80.00</h5>
                                        <div class="text-right">
                                            <a href="#"><i class="fas fa-minus sub pr5"></i></a>
                                            <span class="count">1</span>
                                            <a href="#"><i class="fas fa-plus add pl5"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="pos-rel">
                                <input type="text" name="" placeholder="Coupon Code" class="coupon-code" />

                                <div class="btn btn-lg apply-code">Apply</div>

                                <div class="btn btn-lg delete-item">Delete item</div>
                            </div>
                        </div>
                        <div class="col-12 my-2">
                            <h4>Cart Totals</h4>
                        </div>

                        <div class="col-12">
                            <div class="cart-bg">
                                <div class="total">
                                    <div class="row">
                                        <div class="col-4">
                                            <div>Subtotal -</div>
                                        </div>
                                        <div class="col-8">
                                            <div class="text-right">$140.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="total">
                                    <div class="row">
                                        <div class="col-4 col-sm-8">
                                            <div>Shipping -</div>
                                        </div>
                                        <div class="col-8 col-sm-4">
                                            <div><i class="fas fa-circle"></i> Flat rate: $12.00</div>
                                            <div><i class="far fa-circle"></i>Local pickup: $60.00</div>
                                            <div><i class="far fa-circle"></i> Free Shipping</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="g-total">
                                    <div class="row">
                                        <div class="col-4 col-sm-8">
                                            <div>Total -</div>
                                        </div>
                                        <div class="col-8 col-sm-4">
                                            <div class="text-right">$152.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="check-btn">Preceed To Checkout</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /Page Wrapper -->
        </div>

        <!-- /Main Wrapper -->

        <?php include_once('includes/footer.php');?>

    </body>
</html>
