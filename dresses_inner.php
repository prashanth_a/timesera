<!DOCTYPE html>

<html lang="en">
    <?php include_once('includes/header.php');?>

    <style type="text/css">
        .product_bg {
            box-shadow: 1px 1px 17px 1px #b9b9b9;
            border-radius: 5px;
            background-color: #fff;
            padding: 8px;
            cursor: pointer;
        }
        i.fas.fa-star,
        i.fas.fa-star-half-alt {
            color: #ff5200;
        }
        span s {
            font-size: 12px;
            margin-left: 10px;
            color: #757575;
        }
        .pdname_bg {
            padding: 5px 10px;
            background-color: #dedcdc;
            border-radius: 5px;
            text-align: center;
        }
        .pdname_bg h6 {
            font-weight: bold;
            margin-bottom: 0px;
        }
        .pdname {
            font-size: 25px;
            color: #0b0157;
        }
        .pdcolors {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            margin: 5px 0px;
            cursor: pointer;
        }
        .pdclr1 {
            background-color: #0b0b0b;
            box-shadow: 0px 1px 11px 1px #0b0b0b;
        }
        .pdclr1:hover,
        .pdclr1:focus {
            box-shadow: none;
        }
        .pdclr2 {
            background-color: #e85a2f;
            box-shadow: 0px 1px 11px 1px #e85a2f;
        }
        .pdclr2:hover,
        .pdclr2:focus {
            box-shadow: none;
        }
        .pdclr3 {
            background-color: #c9c9c9;
            box-shadow: 0px 1px 11px 1px #c9c9c9;
        }
        .pdclr3:hover,
        .pdclr3:focus {
            box-shadow: none;
        }
        .pdclr4 {
            background-color: #fff;
            box-shadow: 0px 1px 11px 1px #f1f1f1;
        }
        .pdclr4:hover,
        .pdclr4:focus {
            box-shadow: none;
        }
        .pdclr5 {
            background-color: #ff4a4a;
            box-shadow: 0px 1px 11px 1px #ff4a4a;
        }
        .pdclr5:hover,
        .pdclr5:focus {
            box-shadow: none;
        }
        .pdclr6 {
            background-color: #3e3a3a;
            box-shadow: 0px 1px 11px 1px #3e3a3a;
        }
        .pdclr6:hover,
        .pdclr6:focus {
            box-shadow: none;
        }
        .pdsize {
            box-shadow: 0px 1px 11px 1px #f1f1f1;
            text-align: center;
        }
        .pdsize:focus {
            color: #ff5504;
        }
        i.far.fa-heart {
            position: absolute;
            right: 10%;
            top: 3%;
            font-size: 20px;
        }
        .sticky-buttons {
            position: fixed;
            width: 100%;
            margin: 0;
            bottom: -2px;
            z-index: 7;
            display: flex;
        }
        .butns-bg {
            background-color: #fff;
            text-align: center;
            border: 1px solid #d5a249;
            height: 40px;
            line-height: 40px;
            padding: 0px;
        }

        .cart-button {
            background-color: #fff;
            color: #d5a249;
            font-size: 13px;
            margin-bottom: 0;
            text-transform: uppercase;
        }
        .cart-button:hover,
        .cart-button:focus {
            background-color: #d5a249;
            text-align: center;
            border: 1px solid #d5a249;
            color: #fff;
        }
        i.fab.fa-whatsapp {
            font-size: 18px;
        }
    </style>

    <body>
        <!-- Main Wrapper -->

        <div class="main-wrapper">
            <!-- Header -->

            <?php include_once('includes/topbar.php');?>

            <!-- /Header -->

            <!-- Sidebar -->

            <?php include_once('includes/sidebar.php');?>

            <!-- /Sidebar -->

            <!-- Page Wrapper -->

            <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-12 col-md-4 mt15">
                            <div class="product_bg">
                                <div><i class="far fa-heart"></i></div>

                                <div class="text-center">
                                    <img src="images/dresses/dress.png" class="img-fluid" alt="dress" />
                                </div>

                                <div class="px-2">
                                    <div class="mt15 mb5">
                                        <a href="#" class="pdname">Full Slive T-Shirt</a>
                                    </div>

                                    <h5 class="font-weight-bold">Zara T-shirt</h5>

                                    <div>
                                        <span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star-half-alt"></i>
                                        </span>

                                        <span> (195 Reviews)</span>
                                    </div>

                                    <div>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                        aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                                        qui officia deserunt mollit anim id est laborum.
                                    </div>

                                    <div class="text-dark font-weight-bold my-2">SELECT COLOR</div>

                                    <div class="row">
                                        <div class="col-3 col-sm-2 col-md-3">
                                            <div class="pdcolors pdclr1"></div>
                                        </div>
                                        <div class="col-3 col-sm-2 col-md-3">
                                            <div class="pdcolors pdclr2"></div>
                                        </div>
                                        <div class="col-3 col-sm-2 col-md-3">
                                            <div class="pdcolors pdclr3"></div>
                                        </div>
                                        <div class="col-3 col-sm-2 col-md-3">
                                            <div class="pdcolors pdclr4"></div>
                                        </div>
                                        <div class="col-3 col-sm-2 col-md-3">
                                            <div class="pdcolors pdclr5"></div>
                                        </div>
                                        <div class="col-3 col-sm-2 col-md-3">
                                            <div class="pdcolors pdclr6"></div>
                                        </div>
                                    </div>

                                    <div class="text-dark font-weight-bold my-2">SELECT SIZE (US)</div>

                                    <div class="row">
                                        <div class="col-3 col-sm-2 px8">
                                            <div class="pdsize">4.5</div>
                                        </div>
                                        <div class="col-3 col-sm-2 px8">
                                            <div class="pdsize">5.0</div>
                                        </div>
                                        <div class="col-3 col-sm-2 px8">
                                            <div class="pdsize">6.0</div>
                                        </div>
                                        <div class="col-3 col-sm-2 px8">
                                            <div class="pdsize">7.0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sticky-buttons row">
                            <div class="butns-bg col-2">
                                <div class="cart-button">
                                    <span><i class="fab fa-whatsapp"></i></span>
                                </div>
                            </div>

                            <div class="butns-bg col-5">
                                <div class="cart-button">
                                    <span>save to closet</span>
                                </div>
                            </div>
                            <div class="butns-bg col-5">
                                <div class="cart-button">
                                    <span>Add To Cart</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /Page Wrapper -->
                </div>
            </div>

            <!-- /Main Wrapper -->
        </div>

        <?php include_once('includes/footer.php');?>

    </body>
</html>
