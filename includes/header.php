<head>
        <meta charset="utf-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />

        <title>TimesEra</title>

        <!-- Favicon -->

        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

        <!-- Bootstrap CSS -->

        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />

        <!-- Fontawesome CSS -->

        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

        <!-- Feathericon CSS -->

        <link rel="stylesheet" href="assets/css/feathericon.min.css" />

        <link href="https://fonts.googleapis.com/css2?family=Play&display=swap" rel="stylesheet" />

        <!-- Main CSS -->

        <link rel="stylesheet" href="assets/css/style.css" />

        <link rel="stylesheet" type="text/css" href="assets/css/custom.css" />

        <link rel="stylesheet" type="text/css" href="assets/css/veena.css" />

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" />
    </head>