<div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                            <li class="active">
                                <a href="#">
                                    <i class="fas fa-male"></i>
                                    <span>Mens</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fas fa-female"></i>
                                    <span>Womens</span>
                                </a>
                            </li>

                            <li class="submenu">
                                <a href="#">
                                    <i class="far fa-gem" style="font-size: 15px !important;"></i>
                                    <span> Jewellery</span>
                                    <span class="menu-arrow"></span>
                                </a>

                                <ul style="display: none;">
                                    <li>
                                        <a href="#">
                                            <i class="far fa-gem" style="font-size: 15px !important;"></i>
                                            <span>Diamonds</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- <li class="submenu">
                                <a href="#"><i class="fe fe-document"></i> <span> Authentication </span> <span class="menu-arrow"></span></a>
                                <ul style="display: none;">
                                    <li><a href="login.html"> Login </a></li>
                                    <li><a href="register.html"> Register </a></li>
                                    <li><a href="forgot-password.html"> Forgot Password </a></li>
                                    <li><a href="lock-screen.html"> Lock Screen </a></li>
                                </ul>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>