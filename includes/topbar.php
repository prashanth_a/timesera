<div class="header">
                <!-- Logo -->

                <div class="header-left">
                    <a href="index.php" class="logo">
                        <div class="logo-name">Times Era</div>
                    </a>

                    <a href="index.php" class="logo logo-small">
                        <!-- <img src="images/favicon.png" alt="Logo" width="30" height="30"> -->

                        <div class="logo-name-small">Times Era</div>
                    </a>
                </div>

                <!-- /Logo -->

                <a href="javascript:void(0);" id="toggle_btn">
                    <i class="fe fe-text-align-left"></i>
                </a>

                <div class="cart">
                    <a href="checkout_review.php">
                        <img src="images/cart.png" alt="cart" width="30px" />
                    </a>
                </div>

                <div class="col-12 col-md-12 col-lg-3 form-group has-search mb-0">
                    <span class="fa fa-search form-control-feedback searchh"></span>
                    <input type="text" class="form-control pl30" placeholder="Location" />
                </div>

                <!-- Mobile Menu Toggle -->

                <a class="mobile_btn" id="mobile_btn">
                    <i class="fa fa-bars"></i>
                </a>

                <!-- /Mobile Menu Toggle -->
            </div>