<!DOCTYPE html>

<html lang="en">
    
    <?php include_once('includes/header.php');?>

    <style type="text/css">
        .category-bg {
            box-shadow: 1px 1px 17px 1px #b9b9b9;
            border-radius: 15px;
            background-color: #fff;
            padding: 0px 8px;
            margin: 5px 0px;
        }
        .cat-text {
            font-weight: bold;
            padding-bottom: 12px;
            margin-bottom: 0px;
        }
        .cat-name {
            vertical-align: middle;
            margin: auto;
            font-size: 10px;
            line-height: 12px;
        }
        .cat-img {
            height: 90px;
            display: block;
            margin: auto;
        }
        .brand-logo {
            border: 1px solid #ccc;
            padding: 5px;
            border-radius: 20px;
            width: 100%;
        }
        .brand-logo img {
            /* width: 45px;*/
            margin: auto;
            display: block;
        }
        .brand-ad {
            font-size: 14px;
            line-height: 5px;
            color: #bdbcbc;
        }
        .area {
            text-align: center;
            padding: 5px 10px;
            border: 1px solid #ddd;
            font-size: 10px;
            border-radius: 5px;
            color: #000;
        }
        .percent {
            background-color: #a4d4aa;
            border-radius: 20px;
            color: #fff;
            font-size: 10px;
            padding: 3px 4px;
        }
        .code {
            font-size: 12px;
            color: #a4d4aa;
        }
        .lh40 {
            line-height: 40px;
        }
        .fur-ban {
            border-radius: 10px;
        }
        .border-grey {
            border: 1px solid #ccc;
        }
        .border-right-grey {
            border-right: 1px solid #ccc;
        }
        .border-left-grey {
            border-left: 1px solid #ccc;
        }
        .border-bottom-grey {
            border-bottom: 1px solid #ccc;
        }
        .p15 {
            padding: 15px;
        }
        @media (min-width: 568px) and (max-width: 991px) {
        }
    </style>

    <body>
        <!-- Main Wrapper -->

        <div class="main-wrapper">
            <!-- Header -->

            <?php include_once('includes/topbar.php');?>

            <!-- /Header -->

            <!-- Sidebar -->

            <?php include_once('includes/sidebar.php');?>

            <!-- /Sidebar -->

            <!-- Page Wrapper -->

            <div class="page-wrapper">
                <div class="content container-fluid px-0">
                    <div id="demo" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->

                        <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>

                            <li data-target="#demo" data-slide-to="1"></li>

                            <li data-target="#demo" data-slide-to="2"></li>

                            <li data-target="#demo" data-slide-to="3"></li>

                            <li data-target="#demo" data-slide-to="4"></li>

                            <li data-target="#demo" data-slide-to="5"></li>
                        </ul>

                        <!-- The slideshow -->

                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="images/banner1.jpg" alt="banner1" />
                            </div>

                            <div class="carousel-item">
                                <img src="images/banner2.jpg" alt="banner2" />
                            </div>

                            <div class="carousel-item">
                                <img src="images/banner3.jpg" alt="banner3" />
                            </div>

                            <div class="carousel-item">
                                <img src="images/banner4.jpg" alt="banner4" />
                            </div>

                            <div class="carousel-item">
                                <img src="images/banner5.jpg" alt="banner5" />
                            </div>

                            <div class="carousel-item">
                                <img src="images/banner6.jpg" alt="banner6" />
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <h5 class="cat-text">Categories</h5>
                    </div>

                    <div class="col-12">
                        <div class="row px8">
                            <div class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>MENS</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/men.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>WOMENS</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/women.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>KIDS</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/kids.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div href="#" class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>JEWLLERY</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/jewellery.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div href="#" class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>BEAUTY</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/beauty.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div href="#" class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>IT PRODUCTS</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/it-products.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div href="#" class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>MOBILES</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/mobile.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div href="#" class="col-6 col-sm-4 col-md-3 px8">
                                <div class="category-bg">
                                    <div class="row">
                                        <div class="col-5 cat-name">
                                            <div>IT SALES & SERVICES</div>
                                        </div>
                                        <div class="col-7 px-0">
                                            <img src="images/services.png" class="img-fluid cat-img" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div id="demo" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->

                                <ul class="carousel-indicators">
                                    <li data-target="#demo" data-slide-to="0" class="active"></li>

                                    <li data-target="#demo" data-slide-to="1"></li>

                                    <li data-target="#demo" data-slide-to="2"></li>
                                </ul>

                                <!-- The slideshow -->

                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="images/banner1.jpg" alt="banner1" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/banner2.jpg" alt="banner2" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/banner1.jpg" alt="banner3" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <h5 class="mb15 font-weight-bold">Newly Added Stores</h5>
                    </div>
                    <div class="col-12 my-2">
                        <div class="row">
                            <div class="col-3">
                                <div class="brand-logo">
                                    <img src="images/logos/ajio-logo.png" alt="ajio logo" class="img-fluid" />
                                </div>
                            </div>
                            <div class="col-9 px-0">
                                <h5>AJIO</h5>
                                <div class="brand-ad">Best Fashion & Lifestyle Brands</div>
                                <div class="lh40">
                                    <span class="percent">%</span>
                                    <span class="code">FLAT 200 OFF JUSTCODE:PPFIRST</span>
                                </div>
                            </div>
                            <!-- <div class="col-3 pl-0">
                                <div class="area">
                                    <div>Not available in your area</div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-12 my-2">
                        <div class="row">
                            <div class="col-3">
                                <div class="brand-logo">
                                    <img src="images/logos/aldo-logo.png" alt="ajio logo" class="img-fluid" />
                                </div>
                            </div>
                            <div class="col-9 px-0 pt-2">
                                <h5>Aldo Shoes</h5>
                                <div class="brand-ad">Stylish Footwear & Handbags</div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12 my-2">
                        <div class="row">
                            <div class="col-3">
                                <div class="brand-logo">
                                    <img src="images/logos/beardo-logo.png" alt="ajio logo" class="img-fluid" />
                                </div>
                            </div>
                            <div class="col-6 px-0 pt-2">
                                <h5>Beardo</h5>
                                <div class="brand-ad">Shop for the men Grooming Products</div>
                            </div>
                            <div class="col-3 pl-0">
                                <div class="area">
                                    <div>Not available in your area</div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="col-12">
                        <div>
                            <img src="images/furniture-banner.jpg" class="img-fluid fur-ban" />
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div id="demo" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->

                                <ul class="carousel-indicators">
                                    <li data-target="#demo" data-slide-to="0" class="active"></li>

                                    <li data-target="#demo" data-slide-to="1"></li>

                                    <li data-target="#demo" data-slide-to="2"></li>

                                    <li data-target="#demo" data-slide-to="3"></li>

                                    <li data-target="#demo" data-slide-to="4"></li>

                                    <li data-target="#demo" data-slide-to="5"></li>

                                    <li data-target="#demo" data-slide-to="6"></li>

                                    <li data-target="#demo" data-slide-to="7"></li>

                                    <li data-target="#demo" data-slide-to="8"></li>

                                    <li data-target="#demo" data-slide-to="9"></li>
                                </ul>

                                <!-- The slideshow -->

                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="images/sale-banner.jpg" alt="banner1" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner1.jpg" alt="banner2" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner2.jpg" alt="banner3" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner3.jpg" alt="banner4" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner4.jpg" alt="banner5" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner5.jpg" alt="banner6" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner.jpg" alt="banner1" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner1.jpg" alt="banner2" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner2.jpg" alt="banner3" />
                                    </div>

                                    <div class="carousel-item">
                                        <img src="images/sale-banner3.jpg" alt="banner4" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <h5 class="mb15 font-weight-bold">IT Products</h5>
                    </div>

                    <div id="demo" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->

                        <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>

                            <li data-target="#demo" data-slide-to="1"></li>
                        </ul>

                        <!-- The slideshow -->

                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="col-12">
                                    <div class="bg-white border-grey">
                                        <div class="row">
                                            <div class="col-6 pr-0">
                                                <div class="p15 border-right-grey border-bottom-grey">
                                                    <img src="images/laptops/lap1.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>
                                            <div class="col-6 pl-0">
                                                <div class="p15 border-left-grey border-bottom-grey">
                                                    <img src="images/laptops/lap2.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>
                                            <div class="col-6 pr-0">
                                                <div class="p15 border-right-grey">
                                                    <img src="images/laptops/lap3.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>

                                            <div class="col-6 pl-0">
                                                <div class="p15 border-left-grey">
                                                    <img src="images/laptops/lap4.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="carousel-item">
                                <div class="col-12">
                                    <div class="bg-white border-grey">
                                        <div class="row">
                                            <div class="col-6 pr-0">
                                                <div class="p15 border-right-grey border-bottom-grey">
                                                    <img src="images/printers/printer1.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>
                                            <div class="col-6 pl-0">
                                                <div class="p15 border-left-grey border-bottom-grey">
                                                    <img src="images/printers/printer2.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>
                                            <div class="col-6 pr-0">
                                                <div class="p15 border-right-grey">
                                                    <img src="images/printers/printer1.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>

                                            <div class="col-6 pl-0">
                                                <div class="p15 border-left-grey">
                                                    <img src="images/printers/printer2.png" class="img-fluid" alt="laptop1" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /Page Wrapper -->
        </div>

        <!-- /Main Wrapper -->

        <?php include_once('includes/footer.php');?>
        
    </body>
</html>
