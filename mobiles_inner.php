<!DOCTYPE html>

<html lang="en">
    
    <?php include_once('includes/header.php');?>

    <style type="text/css">
        .carousel-inner img {
            width: auto;
            height: 300px;
            padding: 15px 15px 15px 15px;
            border-radius: 25px;
            display: block;
            margin: 0px auto;
        }
        .carousel-indicators li {
            width: 8px !important;
            height: 8px !important;
        }
        .carousel-indicators {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            border-radius: 20px;
            height: 0px;
            bottom: 0px;
        }
        .carousel-indicators li {
            width: 8px !important;
            height: 8px !important;
            border: 1px solid #ff5200;
            background: transparent;
        }
        .product_bg {
            box-shadow: 1px 1px 17px 1px #b9b9b9;
            border-radius: 5px;
            background-color: #fff;
            padding: 8px;
        }
        i.fas.fa-star,
        i.fas.fa-star-half-alt {
            color: #ff5200;
        }
        span s {
            font-size: 12px;
            margin-left: 10px;
            color: #757575;
        }
        .pdname_bg {
            padding: 5px 10px;
            background-color: #dedcdc;
            border-radius: 5px;
            text-align: center;
        }
        .pdname_bg h6 {
            font-weight: bold;
            margin-bottom: 0px;
        }
        .tip-text {
            font-size: 12px;
            color: #757575;
        }
        i.far.fa-heart {
            position: absolute;
            right: 10%;
            top: 3%;
            font-size: 20px;
        }
        .butn {
            width: 125px;
            color: #00d0f1;
            border: 1px solid #00d0f1;
            border-radius: 5px;
            background: transparent;
        }
        .butn:hover {
            background-color: #00d0f1;
            color: #fff;
        }

        .drpdownMenubtn {
            background: transparent;
            border: none;
            color: #00d0f1;
        }

        .drpdownMenubtn:hover,
        .drpdownMenubtn:focus {
            background: transparent;
            border: none;
            color: #00d0f1;
        }

        .drpdownMenubtn::after {
            font-family: Fontawesome !important;
            content: "\f107" !important;
            border: none;
            margin-left: 5px;
            position: absolute;
        }

        .drpdwn.show {
            border: none;
            width: 100%;
            text-align: left;
            /*left: -70px !important;*/
        }

        .drpdwn-item {
            white-space: pre-wrap;
        }
    </style>

    <body>
        <!-- Main Wrapper -->

        <div class="main-wrapper">
            <!-- Header -->

            <?php include_once('includes/topbar.php');?>

            <!-- /Header -->

            <!-- Sidebar -->

            <?php include_once('includes/sidebar.php');?>

            <!-- /Sidebar -->

            <!-- Page Wrapper -->

            <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-12 col-md-4 mt15">
                            <div class="product_bg">
                                <div><i class="far fa-heart"></i></div>

                                <div id="demo" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->

                                    <ul class="carousel-indicators mb-0">
                                        <li data-target="#demo" data-slide-to="0" class="active"></li>

                                        <li data-target="#demo" data-slide-to="1"></li>

                                        <li data-target="#demo" data-slide-to="2"></li>

                                        <li data-target="#demo" data-slide-to="3"></li>
                                    </ul>

                                    <!-- The slideshow -->

                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="images/mobiles/screen1.png" alt="screen1" />
                                        </div>

                                        <div class="carousel-item">
                                            <img src="images/mobiles/screen2.png" alt="mobile-screen1" />
                                        </div>

                                        <div class="carousel-item">
                                            <img src="images/mobiles/screen3.png" alt="" />
                                        </div>

                                        <div class="carousel-item">
                                            <img src="images/mobiles/screen4.png" alt="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="px-2">
                                    <div class="mt15 mb5">
                                        <a href="#">Apple</a>
                                    </div>

                                    <h5 class="font-weight-bold">iPhone 7 plus (128 GB)</h5>

                                    <div>
                                        <span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star-half-alt"></i>
                                        </span>

                                        <span>(195 Reviews)</span>
                                    </div>

                                    <div class="mt5">
                                        <span>
                                            <a href="#">&#8377; 60,000</a>
                                        </span>
                                        <span><s>&#8377; 90,000</s></span>
                                    </div>
                                </div>

                                <div class="col-12 mt10">
                                    <div class="row">
                                        <div class="col-4 px8">
                                            <div class="pdname_bg">
                                                <h6>Gold</h6>
                                                <div class="tip-text">gold</div>
                                            </div>
                                        </div>

                                        <div class="col-4 px8">
                                            <div class="pdname_bg">
                                                <h6>128 GB</h6>
                                                <div class="tip-text">ROM</div>
                                            </div>
                                        </div>

                                        <div class="col-4 px8">
                                            <div class="pdname_bg">
                                                <h6>3 GB</h6>
                                                <div class="tip-text">RAM</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle drpdownMenubtn pt-3" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Specifications
                                        </button>
                                        <div class="dropdown-menu drpdwn" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item drpdwn-item" href="#">Internal: 32GB 3GB RAM, 128GB 3GB RAM, 256GB 3GB RAM</a>
                                            <a class="dropdown-item drpdwn-item" href="#">Size: 5.5 inches, 83.4 cm2 (~67.7% screen-to-body ratio)</a>
                                            <a class="dropdown-item drpdwn-item" href="#">Resolution: 1080 x 1920 pixels, 16:9 ratio (~401 ppi density)</a>
                                        </div>
                                    </div>

                                    <div class="row py-3">
                                        <div class="col-6 px8">
                                            <div class="text-right">
                                                <input type="button" class="butn" name="" value="Add to cart" />
                                            </div>
                                        </div>

                                        <div class="col-6 px8">
                                            <div class="text-left">
                                                <input type="button" class="butn" name="" value="Buy Now" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /Page Wrapper -->
        </div>

        <!-- /Main Wrapper -->

        <?php include_once('includes/footer.php');?>

    </body>
</html>
