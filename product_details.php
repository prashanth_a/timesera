<!DOCTYPE html>

<html lang="en">

    <?php include_once('includes/header.php');?>

    <style type="text/css">
        .carousel-inner img {
            width: auto;
            height: 370px;
            padding: 15px 15px 15px 15px;
            border-radius: 25px;
            display: block;
            margin: 0px auto;
        }
        .carousel-indicators {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            border-radius: 20px;
            height: 0px;
            bottom: 0px;
        }
        .carousel-indicators li {
            width: 60px !important;
            height: 0px !important;
            border: 1px solid #dadada !important;;
            background-color: #dadada !important;
        }
        .carousel-indicators .active {
            background-color: #ff5200 !important;
            border: 1px solid #ff5200 !important;
        }
        .product_bg {
            box-shadow: 1px 1px 17px 1px #b9b9b9;
            border-radius: 5px;
            background-color: #fff;
            padding: 8px;
        }
        i.fas.fa-star,
        i.fas.fa-star-half-alt {
            color: #ff5200;
        }
        span s {
            font-size: 12px;
            margin-left: 10px;
            color: #757575;
        }
        .pdcolors {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            margin: 5px 0px;
            cursor: pointer;
        }
        .pdclr1 {
            background-color: #0b0b0b;
            box-shadow: 0px 1px 11px 1px #0b0b0b;
        }
        .pdclr1:hover,
        .pdclr1:focus {
            box-shadow: none;
        }
        .pdclr2 {
            background-color: #e85a2f;
            box-shadow: 0px 1px 11px 1px #e85a2f;
        }
        .pdclr2:hover,
        .pdclr2:focus {
            box-shadow: none;
        }
        .pdclr3 {
            background-color: #c9c9c9;
            box-shadow: 0px 1px 11px 1px #c9c9c9;
        }
        .pdclr3:hover,
        .pdclr3:focus {
            box-shadow: none;
        }
        .pdclr4 {
            background-color: #fff;
            box-shadow: 0px 1px 11px 1px #f1f1f1;
        }
        .pdclr4:hover,
        .pdclr4:focus {
            box-shadow: none;
        }
        .pdclr5 {
            background-color: #ff4a4a;
            box-shadow: 0px 1px 11px 1px #ff4a4a;
        }
        .pdclr5:hover,
        .pdclr5:focus {
            box-shadow: none;
        }
        .pdclr6 {
            background-color: #3e3a3a;
            box-shadow: 0px 1px 11px 1px #3e3a3a;
        }
        .pdclr6:hover,
        .pdclr6:focus {
            box-shadow: none;
        }
        .pdname_bg {
            padding: 5px 10px;
            background-color: #dedcdc;
            border-radius: 5px;
            text-align: center;
        }
        .pdname_bg h6 {
            font-weight: bold;
            margin-bottom: 0px;
        }
        .tip-text {
            font-size: 12px;
            color: #757575;
        }
        i.far.fa-heart {
            position: absolute;
            right: 10%;
            top: 3%;
            font-size: 20px;
            z-index: 1;
        }
        .sticky-buttons {
            position: fixed;
            width: 100%;
            margin: 0;
            bottom: -2px;
            z-index: 7;
            display: flex;
            margin-left: 0px;
        }
        .butns-bg {
            background-color: #fff;
            text-align: center;
            border: 1px solid #d5a249;
            height: 40px;
            line-height: 40px;
            padding: 0px;
        }

        .cart-button {
            background-color: #fff;
            color: #d5a249;
            font-size: 13px;
            margin-bottom: 0;
            text-transform: uppercase;
        }
        .cart-button:hover,
        .cart-button:focus {
            background-color: #d5a249;
            text-align: center;
            border: 1px solid #d5a249;
            color: #fff;
        }
        i.fab.fa-whatsapp {
            font-size: 18px;
        }

        
    </style>

    <body>
        <!-- Main Wrapper -->

        <div class="main-wrapper">
            <!-- Header -->

            <?php include_once('includes/topbar.php');?>

            <!-- /Header -->

            <!-- Sidebar -->

            <?php include_once('includes/sidebar.php');?>

            <!-- /Sidebar -->

            <!-- Page Wrapper -->

            <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 mt15">
                            <div class="product_bg">
                                <div><i class="far fa-heart"></i></div>

                                <div id="demo" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->

                                    <ul class="carousel-indicators mb-0">
                                        <li data-target="#demo" data-slide-to="0" class="active"></li>

                                        <li data-target="#demo" data-slide-to="1"></li>

                                        <li data-target="#demo" data-slide-to="2"></li>

                                        <li data-target="#demo" data-slide-to="3"></li>
                                    </ul>

                                    <!-- The slideshow -->

                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="images/product-details/image1.jpg" alt="" />
                                        </div>

                                        <div class="carousel-item">
                                            <img src="images/product-details/image2.jpg" alt="" />
                                        </div>

                                        <div class="carousel-item">
                                            <img src="images/product-details/image3.jpg" alt="" />
                                        </div>

                                        <div class="carousel-item">
                                            <img src="images/product-details/image4.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="px-2">

                                    <h5 class="font-weight-bold mt15">T-SHIRT</h5>

                                    <div>
                                        <span>$80.00</span>
                                        <span><s>$99</s></span>
                                    </div>

                                    <div class="text-dark font-weight-bold my-2">SELECT COLOR</div>

                                    <div class="row">
                                        <div class="col-3 col-sm-1 col-md-3">
                                            <div class="pdcolors pdclr1"></div>
                                        </div>
                                        <div class="col-3 col-sm-1 col-md-3">
                                            <div class="pdcolors pdclr2"></div>
                                        </div>
                                        <div class="col-3 col-sm-1 col-md-3">
                                            <div class="pdcolors pdclr3"></div>
                                        </div>
                                        <div class="col-3 col-sm-1 col-md-3">
                                            <div class="pdcolors pdclr4"></div>
                                        </div>
                                        <div class="col-3 col-sm-1 col-md-3">
                                            <div class="pdcolors pdclr5"></div>
                                        </div>
                                        <div class="col-3 col-sm-1 col-md-3">
                                            <div class="pdcolors pdclr6"></div>
                                        </div>
                                    </div>

        
                                </div>
                            </div>
                        </div>
                        <div class="sticky-buttons row">
                            <div class="butns-bg col-2">
                                <div class="cart-button">
                                    <span><i class="fab fa-whatsapp"></i></span>
                                </div>
                            </div>

                            <div class="butns-bg col-5">
                                <div class="cart-button">
                                    <span>save to closet</span>
                                </div>
                            </div>
                            <div class="butns-bg col-5">
                                <div class="cart-button">
                                    <span>Add To Cart</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /Page Wrapper -->
        </div>

        <!-- /Main Wrapper -->

        <?php include_once('includes/footer.php');?>

    </body>
</html>
