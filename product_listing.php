<!DOCTYPE html>

<html lang="en">
    <?php include_once('includes/header.php');?>

    <body>
        <!-- Main Wrapper -->

        <div class="main-wrapper">
            <!-- Header -->

            <?php include_once('includes/topbar.php');?>

            <!-- /Header -->

            <!-- Sidebar -->

            <?php include_once('includes/sidebar.php');?>

            <!-- /Sidebar -->

            <!-- Page Wrapper -->

            <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="filter-bg">
                    <div class="d-flex">
                        <div class="w-50 px-4 filter"><i class="fas fa-sliders-h pr-2"></i>Categories</div>
                    
                        <div class="w-50 px-4 sort"><i class="fas fa-sort-amount-up pr-2"></i>Sort</div>
                    </div>
                </div>

                    <div class="row">
                        <div class="co-12">
                            
                        </div>
                        <div class="col-12">
                            <div class="row px8">
                                <div class="col-6 col-sm-4 col-md-2 px8">
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/men1.png" class="img-fluid" alt="men" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Mens Plain Shirt
                                            </div>
                                            <div class="pdname">
                                               Regular Fit Polo
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                                  <div class="col-6 col-sm-4 col-md-2 px8">
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/headphones.png" class="img-fluid" alt="headphones" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Headphones
                                            </div>
                                            <div class="pdname">
                                               headphones
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                                <div class="col-6 col-sm-4 col-md-2 px8">                                    
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/men1.png" class="img-fluid" alt="men" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Mens Plain Shirt
                                            </div>
                                            <div class="pdname">
                                               Regular Fit Polo
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                                  <div class="col-6 col-sm-4 col-md-2 px8">
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/headphones.png" class="img-fluid" alt="headphones" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Headphones
                                            </div>
                                            <div class="pdname">
                                               headphones
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                                <div class="col-6 col-sm-4 col-md-2 px8">
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/men1.png" class="img-fluid" alt="men" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Mens Plain Shirt
                                            </div>
                                            <div class="pdname">
                                               Regular Fit Polo
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                                  <div class="col-6 col-sm-4 col-md-2 px8">
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/headphones.png" class="img-fluid" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Headphones
                                            </div>
                                            <div class="pdname">
                                               headphones
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                                <div class="col-6 col-sm-4 col-md-2 px8">
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/men1.png" class="img-fluid" alt="men" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Mens Plain Shirt
                                            </div>
                                            <div class="pdname">
                                               Regular Fit Polo
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                                  <div class="col-6 col-sm-4 col-md-2 px8">
                                    <a href="store-details.php">
                                    <div class="category-bg">
                                        <div class="row">
                                            
                                            <div><i class="fas fa-heart"></i></div>
                                            <div class="mx-auto d-block col-10">
                                                <img src="images/headphones.png" class="img-fluid" alt="headphones" />
                                            </div>
                                            <div class="col-12">
                                            <div class="pdname">
                                                Headphones
                                            </div>
                                            <div class="pdname">
                                               headphones
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <span>$8</span>
                                                <span><s>$20</s></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="float-right">
                                               <div class="discount">20%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <span>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half-alt"></i>
                                            </span>
                                            <span class="pl-1 rev">Reviews</span>
                                        </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                            </div>
                        </div>
                        <div class="sticky-buttons row">
                            <div class="butns-bg col-2">
                                <div class="cart-button">
                                    <span><i class="fab fa-whatsapp"></i></span>
                                </div>
                            </div>

                            <div class="butns-bg col-5">
                                <div class="cart-button">
                                    <span>save to closet</span>
                                </div>
                            </div>
                            <div class="butns-bg col-5">
                                <div class="cart-button">
                                    <span>Add To Cart</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /Page Wrapper -->
                </div>
            </div>

            <!-- /Main Wrapper -->
        </div>

        <?php include_once('includes/footer.php');?>

    </body>
</html>
