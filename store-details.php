<!DOCTYPE html>

<html lang="en">
    <?php include_once('includes/header.php');?>
    <body>
        <!-- Main Wrapper -->

        <div class="main-wrapper">
            <!-- Header -->

            <?php include_once('includes/topbar.php');?>

            <!-- /Header -->

            <!-- Sidebar -->

            <?php include_once('includes/sidebar.php');?>

            <!-- /Sidebar -->

            <!-- Page Wrapper -->

            <div class="page-wrapper">
            	<div class="container-fluid">
	            	<div class="d-flex ai-center bg-white border mt-2 mb-2 p-2">
	            		<div class="store_icon">
	            			<img src="images/stores/klm.jpg" class="img-fluid rounded-circle">
	            		</div>
	            		<div class="store_details text-dark">
	            			<h3 class="bold font-weight-600 mb-0">KLM Fashion Mall</h3>
	            			<p class="mb-0">Kukatpally, Hyderabad</p>
	            			<p class="mb-0">
	            				<span>
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star-half-alt"></i>
	                            </span>
	                            <span class="ml-2 fs12">(20)</span>
	            			</p>
	            		</div>
	            	</div>
	            	<div class="categories">
	            	<div class="categories-section">
	            		<div class="d-flex ai-center bg-white border text-dark item">
	            			<div class="w-65 mr-3">
	            				<img src="images/men.png" class="img-fluid rounded-circle">
	            			</div>
	            			<div>
	            				<p class="mb-0 fs20 font-weight-600">Men</p>
	            			</div>
	            			<div class="text-right ml-auto">
	            				<i class="fa fa-chevron-down fs25"></i>
	            			</div>
	            		</div>
	            		<div class="item-details bg-white border text-dark">
	            			<p>Blazers | Jackets <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            			<p>Jeans <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            			<p>Shirts <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            		</div>
	            	</div>
	            	<div class="categories-section">
	            		<div class="d-flex ai-center bg-white border text-dark item">
	            			<div class="w-65 mr-3">
	            				<img src="images/women.png" class="img-fluid rounded-circle">
	            			</div>
	            			<div>
	            				<p class="mb-0 fs20 font-weight-600">Women</p>
	            			</div>
	            			<div class="text-right ml-auto">
	            				<i class="fa fa-chevron-down fs25"></i>
	            			</div>
	            		</div>
	            		<div class="item-details bg-white border text-dark">
	            			<p>Blazers | Jackets <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            			<p>Dresses | Jumpsuits <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            			<p>Shirts | Tops <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            		</div>
	            	</div>
	            	<div class="categories-section">
	            		<div class="d-flex ai-center bg-white border text-dark item">
	            			<div class="w-65 mr-3">
	            				<img src="images/kids.png" class="img-fluid rounded-circle">
	            			</div>
	            			<div>
	            				<p class="mb-0 fs20 font-weight-600">Kids</p>
	            			</div>
	            			<div class="text-right ml-auto">
	            				<i class="fa fa-chevron-down fs25"></i>
	            			</div>
	            		</div>
	            		<div class="item-details bg-white border text-dark">
	            			<p>Jeans <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            			<p>Shirts <span class="float-right"><i class="fa fa-chevron-right"></i></span></p>
	            		</div>
	            	</div>
	            	</div>
	            	<div class="address mt-3 text-dark">
	            		<h4 class="font-weight-600">Address</h4>
	            		<div class="border bg-white text-dark mt-2 mb-2 theme-shadow">
	            			<p><i class="fa fa-map-marker-alt"></i> MIG. 349 & 350, Balaji Nagar Main Rd, Kukatpally Housing Board Colony, Kukatpally, Hyderabad, Telangana 500072</p>
		            		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3805.2567006420686!2d78.39523841448813!3d17.495255388014073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9142653c9f09%3A0xd7185c165a8a1477!2sKLM%20Fashion%20Mall!5e0!3m2!1sen!2sin!4v1595758302793!5m2!1sen!2sin" width="100%" height="250" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		            	</div>
	            	</div>
	            	<div class="contact mt-3 text-dark">
	            		<h4 class="font-weight-600">Contact</h4>
	            		<div class="border bg-white mt-2 mb-2 p-2 theme-shadow">
	            			<p><a href="tel:+914048522966" class="text-dark"><i class="fa fa-phone-square-alt"></i> 040 48522966</a></p>
	            			<p><a href="mailto:support@klmfashionmall.com" class="text-dark"><i class="fa fa-envelope"></i> support@klmfashionmall.com</a></p>
	            			<p><a href="https://klmfashionmall.com/" class="text-dark"><i class="fa fa-globe"></i> www.klmfashionmall.com</a></p>
	            		</div>
	            	</div>
            	</div>
            </div>
        </div>
    <?php include_once('includes/footer.php');?>

    <script type="text/javascript">
    	$(document).ready(function(){
    		$("body").on("click", ".categories-section .item", function(){
    			// alert()
    			// $(".item-details").slideUp();
    			$(this).parent().find(".item-details").slideToggle();
    		});
    	});
    </script>

    </body>
</html>